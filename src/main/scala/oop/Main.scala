package oop

import oop.ImageFunctions.{Rotate, defaultRotate, flipByX, flipByY}

object Main {

  def main(args: Array[String]): Unit = {
    Filters.addFilters(args)
    phototest()
    println("END")
  }


  def phototest(): Unit = {
    val img = FileHandler.readImageFromFile()

    val image : ImageImmutable = new ImageImmutable(img)
    val pixels: Array[Int] =  image.getColorArrayAsInt

    val str:String =  ASCIIConverter.pixelsToString(image.getWidth,pixels)

    var str1: String = ""

    str1 = Rotate(str,image.getHeight,image.getWidth,false )

    if(Filters.noRotation()) {
      str1 = defaultRotate(str1, image.getWidth, image.getHeight, true)
    } else {
      str1 = defaultRotate(str1, image.getWidth, image.getHeight, false)

      if(Filters.flipX().equals(true) && Filters.flipY().equals(true)) {
        str1 = flipByX(str1,image.getWidth,image.getHeight,false)
        str1 = flipByY(str1,image.getWidth,image.getHeight,true)
      }
      else if(Filters.flipX().equals(true))
        str1 = flipByX(str1,image.getWidth,image.getHeight,true)
      else if(Filters.flipY().equals(true))
        str1 = flipByY(str1,image.getWidth,image.getHeight,true)
    }

    val writeToFileName:String = FileHandler.getFileName

    if(!writeToFileName.equals(""))
      FileHandler.writeTextToFile(writeToFileName,str1)

    if(Filters.writeToConsole())
      FileHandler.writeToConsole(str1)

  }



}
