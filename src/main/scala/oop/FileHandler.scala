package oop

import java.awt.image.BufferedImage
import java.io._
import javax.imageio.ImageIO


object FileHandler {
  def readImageFromFile(): BufferedImage  = {
    val imageFileName:String = Filters.Image()

    if(imageFileName.contains(".png") || imageFileName.contains(".jpg")) {
      val image:BufferedImage = ImageIO.read(new File("src/main/" + imageFileName))
      if(image == null)
        throw new IllegalArgumentException("")
      else
        image
    }
    else
      throw new IllegalArgumentException("")
  }

  // Function is used to write output to the file
  def writeTextToFile(filename: String, s: String): Unit = {
    val file = new File(filename)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(s)
    bw.close()
  }

  def writeToConsole(text:String):Unit = print(text)

  def getFileName : String =  Filters.saveImageToFile()

}
