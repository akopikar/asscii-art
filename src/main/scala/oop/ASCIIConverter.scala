package oop

object ASCIIConverter {
  val chars: String = "$@B%8&M#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/|)1}[]?-_+~>i!lI;:,^`'. "

  def pixelsToString(width:Int, pixels: Array[Int]) : String = {
    var wid: Int = width - 1
    var str: String = ""

    for(i <- 0 until  pixels.length) {
      val tmp : Int = (pixels(i) / 262145).abs;
      str += chars.reverse(tmp);
      if(i == wid)
        wid += width
    }

    str
  }

}
