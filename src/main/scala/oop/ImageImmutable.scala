package oop

import java.awt.Color
import java.awt.image.BufferedImage

class ImageImmutable(image: BufferedImage) {
  var img: BufferedImage = image

  var width: Int = image.getWidth
  var height: Int = image.getHeight

  var pixelsGrid = new PixelsGrid(img,width,height);

  def getWidth : Int = width

  def getHeight : Int = height

  def setWidth( width: Int) : Unit = this.width = width

  def setHeight( height: Int) : Unit = this.height = height

  def getPixelColor(x: Int, y: Int): Color = pixelsGrid.getPixelColor(x,y)

  def getColorArrayAsInt: Array[Int] = {
    pixelsGrid.getColorArrayAsInt;
  }
}

class PixelsGrid(image:BufferedImage, width:Int,height:Int) {
  val pixels: Array[Int] = new Array[Int](width * height)

  def getColorArrayAsInt: Array[Int] = {
    var counter:Int = 0;

    for (x <- 0 until width) {
      for (y <- 0 until height) {
        val col: Color = getPixelColor(x,y)

        val offBrightness = Filters.Brightness()

        val red: Int = (col.getRed * 0.3).toInt
        val green: Int = (col.getGreen * 0.59).toInt
        val blue: Int = (col.getBlue * 0.11).toInt

        var color : Int = 0

        if(red + green + blue + offBrightness  > 255)
          color = 255
        else if( red + green + blue + offBrightness  < 0)
          color = 0
        else
          color = red + green + blue + offBrightness

        if(Filters.Invert())
          color = 255 - color;

        val ocl: Color = new Color(color,color,color);

        pixels(counter) = ocl.getRGB
        counter = counter + 1;
      }
    }
    pixels
  }

  def getPixelColor(x: Int, y: Int) : Color =  new Color(image.getRGB(x,y))
}


