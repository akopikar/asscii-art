package oop

object ImageFunctions {
  def flipByY( pixels_buffer: String, width: Int, height:Int, flag: Boolean): String ={
    var temp_row:String = ""

    for (i <-  1 to height) {
      for (j <- i * width - 1 to  i * width - width by -1) {
        temp_row += pixels_buffer(j)
      }
      if (flag)
        temp_row += "\n"
    }

    temp_row
  }


  def flipByX( pixels_buffer: String, width: Int, height:Int, flag:Boolean): String ={
    var temp_row:String = ""

    for ( i <- height until 0 by -1) {
      for ( j <- i * width - width until i * width)
        temp_row += pixels_buffer(j)
      if(flag)
        temp_row += "\n"
    }

    temp_row;
  }


  def Rotate( pixels_buffer: String, width: Int, height:Int, flag:Boolean): String ={
    var temp_row:String = ""

    for(i <- 1 to width) {
      for(j <- width*height - i to 0 by -width)
        temp_row += pixels_buffer(j)
      if(flag)
        temp_row += "\n"
    }

    temp_row
  }


  def defaultRotate( pixels_buffer: String, width: Int, height:Int, flag:Boolean): String ={
    var temp_row = flipByX(pixels_buffer,width,height,false)
    temp_row = flipByY(temp_row,width,height,flag )

    temp_row;
  }



}
