package oop


// This object is used for filters
object Filters {
  var filters =  List[String]();

  // Get filters form arguments passed by main
  def addFilters(args: Array[String]): Unit = {
    filters = args.toList;
  }

  // Get image path from cmd
  def Image(): String = {
    var str:String = ""

    for (i <- 0 until filters.length)
      if(filters(i).equals("--image"))
        return filters(i + 1)

    throw new IllegalArgumentException("No File");
  }

  // Get save path from cmd
  def saveImageToFile(): String = {
    for (i <- 0 until filters.length)
      if(filters(i).equals("--output-file"))
        return filters(i + 1)
    ""
  }

  // Get if User wants to get the output in console
  def writeToConsole(): Boolean = {
    for (i <- 0 until filters.length)
      if(filters(i).equals("--output-console"))
        return true
    false
  }

  // Get invert filter from cmd
  def Invert(): Boolean = {
    for (i <- 0 until filters.length)
      if(filters(i).equals("--invert"))
        return true
    false
  }

  // Get Brightness filter from cmd
  def Brightness(): Int ={
    for (i <- 0 until filters.length)
      if(filters(i).equals("--brightness"))
        return filters(i + 1).toInt
    0
  }

  // Get if there is no rotation filter from cmd
  def noRotation(): Boolean = {
    for (i <- 0 until filters.length)
      if(filters(i).equals("--flip"))
        return false
    true
  }

  // Get flip by X filter from cmd
  def flipX(): Boolean = {
    for (i <- 0 until filters.length)
      if(filters(i).equals("--flip")) {
        if(filters(i + 1).equals("x"))
        return true
      }
    false
  }

  // Get flip by Y filter from cmd
  def flipY(): Boolean = {
    for (i <- 0 until filters.length)
      if(filters(i).equals("--flip")) {
        if(filters(i+1).equals("y"))
        return true
      }
  false
  }

}
